package upmc.binpacking.bins;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import upmc.binpacking.exceptions.InvalidDimensionException;


public abstract class Bins 
{
	
	private ArrayList<Bin> bins;
	
	private ArrayList<Integer> capacityBins;
	
	int numberOfDimensions;
	
	public abstract Bin instanceBin(ArrayList<Integer> capacity);

	public Bins(String binFilePath, int numberOfDimensions)
	{
		readBinFile(binFilePath,numberOfDimensions);
		bins = new ArrayList<>();
	}
	
	 public void readBinFile (String binFilePath, int numberOfDimensions)            
    {
        try (BufferedReader br = new BufferedReader(new FileReader(binFilePath)))
		{                 
                String sCurrentLine;
                while ((sCurrentLine = br.readLine()) != null) 
                        {
                        ArrayList<Integer> newCapacity = new ArrayList<>();
                        sCurrentLine = sCurrentLine.trim();
                        if (!sCurrentLine.startsWith("#") && !sCurrentLine.startsWith("%")&& !sCurrentLine.isEmpty())
                            {
                            String delims = "\\s+";
                            String[] values = sCurrentLine.split(delims);
                            if(numberOfDimensions>values.length)
                            {
                                throw new InvalidDimensionException("Attempt to introduce incorrect number of dimensions in BIN " );                            
                            } else if (numberOfDimensions<values.length)
                            {
                                System.out.println("WARNING: more dimensions than needed in Bin file.");
                            }
                            for (int i=0; i<numberOfDimensions;i++)
                            {
                                newCapacity.add(Integer.parseInt(values[i]));
                            }
								setCapacityBins(newCapacity);
                            break;
                            }
                        }
 
		} catch (Exception e) 
                {
			e.printStackTrace();
		}
    }
	 
	 
	public ArrayList<Integer> getCapacityBins()
    {
        return capacityBins;
    }
	
	protected void setCapacityBins (ArrayList<Integer> newCapacity)
	{
		capacityBins = newCapacity;
	}
	
	public void addNewBin()
	{	
		Bin bin = instanceBin(capacityBins);
		bins.add(bin);
		//numberOfBins++;
	}
	
	public void removeBin(Bin bin)
	{
		bins.remove(bin);
	}
	
	public void removeBin(int index)
	{
		removeBin(getBin(index));
	}
	
	
	public Bin getBin(int index)
	{
		return bins.get(index);
	}
	
	public int getNumberOfBins()
	{
		return bins.size();
	}
	
	public int getIndexOfBin(Bin bin)
	{
		//System.out.println("DEBUG: indexofbin= "+String.valueOf(bins.indexOf(bin)));
		return bins.indexOf(bin);
	}
	
		
	public ArrayList<Integer> getIndexOfBinsByOccupancyDesc()
	{
	
		ArrayList<Integer> indexes = new ArrayList<>();
		final ArrayList<Double> occupancies = new ArrayList<>();		
		for(int i=0;i<getNumberOfBins();i++)
		{
			indexes.add(i);
			occupancies.add(getBin(i).getOccupancy());			
		}
		
		Collections.sort(indexes, new Comparator<Integer>() {
			@Override public int compare(final Integer o1, final Integer o2) {
				return Double.compare(occupancies.get(o2),occupancies.get(o1));
			}
		});
		
		return indexes;
	}

		
	
}
