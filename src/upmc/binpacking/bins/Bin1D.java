package upmc.binpacking.bins;

import java.util.ArrayList;
import upmc.binpacking.items.Item;
import upmc.binpacking.items.Item1D;
import upmc.binpacking.items.Items1D;

public class Bin1D extends Bin
{  
   
    Bin1D(int capacity)
    {
        super(1);//means NumberOfDimensions = 1
        this.setCapacity(capacity);
		itemsContained = new Items1D();
    }
    
    
    public final void setCapacity (int capacity)
    {
        ArrayList<Integer> newCapacity = new ArrayList<>();
        newCapacity.add(capacity);
        setCapacity(newCapacity);
    }      
    
    public int getCapacityX()
    {
        return getCapacity().get(0);
    }
	
	public void addItem(Item newItem, int positionX)
	{
		ArrayList<Integer> position = new ArrayList<>();
		position.add(positionX);
		super.addItem(newItem, position);
	}
	
	public Boolean isFreePosition(Item1D item, int Position)
	{
		ArrayList<Integer> pos = new ArrayList<>();
		pos.add(Position);
		return super.isFreePosition(item, pos);	
	}



	

}
