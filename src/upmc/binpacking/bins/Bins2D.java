package upmc.binpacking.bins;

import java.util.ArrayList;

public class Bins2D extends Bins{

	public Bins2D(String binFilePath) 
	{
		super(binFilePath, 2); //2 means 2D
	}
	
	public final void setCapacity (int capacityX, int capacityY)
    {
        ArrayList<Integer> newCapacity = new ArrayList<>();
        newCapacity.add(capacityX);
        newCapacity.add(capacityY);
        super.setCapacityBins(newCapacity);
    }
	
	public int getCapacityX()
    {       
        return getCapacityBins().get(0);
	}
    
    public int getCapacityY()
    {
        return getCapacityBins().get(1);
    }
	
	@Override
	public Bin instanceBin(ArrayList<Integer> capacity) 
	{
		return new Bin2D(capacity.get(0),capacity.get(1));
	}


	
	
}
