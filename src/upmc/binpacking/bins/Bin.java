package upmc.binpacking.bins;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import upmc.binpacking.exceptions.InvalidDimensionException;
import upmc.binpacking.exceptions.OverlappingItempsExeption;
import upmc.binpacking.items.Item;
import upmc.binpacking.items.Items;

public abstract class Bin 
{
	
    private final int numberOfDimensions;
    
    private ArrayList<Integer> capacity;
	
	private Integer[] binContent;
	
	protected Items itemsContained;
	
	static public Map<String,ArrayList<Integer>> itemsPositions = new HashMap<>();
	
	static public Map<String,Bin> itemsBinsMap = new HashMap<>();	

	
	    
    Bin(int numberOfDimensions)
    {
        this.numberOfDimensions = numberOfDimensions;
        capacity = new ArrayList<>();
    }
	
	Bin(int numberOfDimensions, ArrayList<Integer> newCapacity)
	{
		this.numberOfDimensions = numberOfDimensions;
        capacity = new ArrayList<>();
		setCapacity(newCapacity);
	}
    
	//For other purposes this should be public
    protected void setCapacity(ArrayList<Integer>  newCapacity)
    {
        try{
            if(newCapacity.size() == numberOfDimensions)
            {
				capacity = newCapacity;
				initBinContent();
            } 
            else
            {
				throw new InvalidDimensionException("Attempt to introduce invalid number of dimensions in Bin ");

            }
        }   
        catch (Exception e) 
        {
                e.printStackTrace();
        }
    }
       
    public ArrayList<Integer> getCapacity()
    {
        return capacity;
    }
	
	public int getVolumen()
	{
		int volumen=1;
		for(int i=0; i<numberOfDimensions;i++)
		{
			volumen=volumen*getCapacity().get(i);			
		}
		return volumen;
	}

    
    public int getNumberOfDimensions()
    {
        return numberOfDimensions;
    }
	
	public void addItem(Item newItem, ArrayList<Integer> position)
	{		
		if(isFreePosition(newItem, position))
		{			
			itemsContained.addItem(newItem);
			placeItem(newItem, position);
			
		} else
		{
			try {
				throw new OverlappingItempsExeption("Cannot add item " + newItem.getID() + " because of overlapping issues");
			} catch (Exception ex) 
			{
				//Logger.getLogger(Bin.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();
			}
		}
	}
	
	public void removeItem(String itemId)
	{
		itemsPositions.remove(itemId);
		int hash = itemsContained.itemsMap.get(itemId);
		for(int i=0;i<this.getVolumen();i++)
		{
			if(binContent[i]==hash)
			{
				binContent[i]= -1;
			}
		}
		
		int index = itemsContained.getIndexOfItem(itemId);
		itemsContained.removeItem(index);
		itemsBinsMap.remove(itemId);
	}
	
	private void initBinContent()
	{		
		//System.out.println("DEBUG : Volumen ="+this.getVolumen());
		binContent = new Integer[this.getVolumen()];
		Arrays.fill(binContent,-1);
	}
	
	public Boolean isFreePosition(Item newItem, ArrayList<Integer> position)
	{
		Boolean isFree = true;
		ArrayList<Integer> currentPosition;
		int index;
		outloop: for(int i=0;i<newItem.getVolumen();i++)
		{
			currentPosition = getPositionOfIndex(i, newItem.getDimensions());
			//System.out.println("DEBUG:posX,posY "+String.valueOf(currentPosition.get(0))+" "+String.valueOf(currentPosition.get(1)));
			currentPosition = sumLists(position, currentPosition);
			index = getIndexOfPosition(currentPosition,capacity);
			for(int j=0;j<numberOfDimensions;j++)
			{
				if (currentPosition.get(j)>= capacity.get(j))
				{
					//System.out.println(currentPosition.get(j));
					isFree = false;
					break outloop;
				}
			}
			if(index>=getVolumen() || index<0)
			{
				isFree = false;
				break;
			} 
			if(binContent[index] != -1)
			{
				isFree = false;
				break;
			}
		}
		
		return isFree;
	}
	
	private ArrayList<Integer> sumLists(ArrayList<Integer> list1,ArrayList<Integer> list2)
	{
		ArrayList<Integer> sum = new ArrayList<>();
		try
		{
			if(list1.size()==list2.size())
			{
				for(int i=0;i<list1.size();i++)
				{
					sum.add(list1.get(i)+list2.get(i));
				}
				
			}
			else
			{				
			throw new InvalidDimensionException("Trying to sum two lists with different dimmensions");
			} 
		}
		catch (InvalidDimensionException ex) 
		{
			//Logger.getLogger(Bin.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		}
		return sum;
		
	}
	
	private int getIndexOfPosition(ArrayList<Integer> position, ArrayList<Integer> dimensions)
	{
		int numberOfDimensions = dimensions.size();
		int index = 0;
		for(int i = 0; i<numberOfDimensions-1;i++)
		{
			int vol = 1;
			for(int j= i+1; j<numberOfDimensions;j++)
			{
				//vol = vol*getCapacity().get(i);
				vol = vol*dimensions.get(j);
			}
			index = index + position.get(i)*vol;
		}
		//System.out.println("DEBUG: numbofdim = "+ String.valueOf(numberOfDimensions));
		index = index + position.get(numberOfDimensions-1);
		return index;
	}

	private ArrayList<Integer> getPositionOfIndex(int index, ArrayList<Integer> dimensions)
	{
		//TODO: tester this function
		int numberOfDimensions = dimensions.size();
		ArrayList<Integer> position = new ArrayList<>();
		int currentAlpha;
		//int i=0;
		//if(numberOfDimensions>1)
		//{
		currentAlpha  = index;
		int prodDim = 1;
		for(int i=1;i<numberOfDimensions;i++)
		{
			prodDim = prodDim*dimensions.get(i);
		}
		currentAlpha = currentAlpha/ prodDim;

		position.add(currentAlpha);
		
		for(int i=1;i<numberOfDimensions;i++)
		{
			currentAlpha = index - position.get(i-1)*prodDim;
			prodDim = prodDim / dimensions.get(i);
			currentAlpha = currentAlpha / prodDim;
			//currentAlpha = index;
			/*for(int j=0;j<i-1;j++)
			//for(int j=i;j<numberOfDimensions;j++)
			{
				currentAlpha = currentAlpha - position.get(j)*dimensions.get(j);
			}*/
			position.add(currentAlpha);
		}
		return position;
	}
	
	private void placeItem(Item item, ArrayList<Integer> position)
	{
		itemsBinsMap.put(item.getID(),this);
		itemsPositions.put(item.getID(), position);
		//Integer.c
		
		//int test = Bin.itemsPositions.get(item.getID()).get(0);
		//System.out.println("DEBUG itemPosition test "+String.valueOf(test));

		ArrayList<Integer> currentPosition;
		int index;
		for(int i=0;i<item.getVolumen();i++)
		{
			currentPosition = getPositionOfIndex(i, item.getDimensions());
			currentPosition = sumLists(position, currentPosition);
			index = getIndexOfPosition(currentPosition,this.capacity);
			//System.out.println("DEBUG: Place Item : Index ="+String.valueOf(itemsContained.itemsMap.get(newItem.getID())));
			binContent[index] = itemsContained.itemsMap.get(item.getID());
			//System.out.println("DEBUG: intemsBinMap cx="+String.valueOf(  itemsBinsMap.get(item.getID()).getCapacity().get(0)  )  );
		}	
	}
	
	
	public double getOccupancy(){		
		
		return (double)(itemsContained.getVolumenOfItems())/(double)(this.getVolumen());
	
	}
	
	public Boolean isEmpty()
	{
		return getOccupancy() == 0;
	}	
	
	public Map<String,ArrayList<Integer>> getPositions()
	{
		return itemsPositions;
	}
	
	public Item getItem(int index)
	{
		return itemsContained.getItem(index);
	}
	
	public int getNumberOfItemsContained()
	{
		return itemsContained.getNumberOfItems();
	}
	
	

}
