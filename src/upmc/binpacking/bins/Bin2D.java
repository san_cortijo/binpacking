package upmc.binpacking.bins;

import java.util.ArrayList;
import upmc.binpacking.items.Item;
import upmc.binpacking.items.Items2D;

public class Bin2D extends Bin {
    
    public Bin2D(int capacityX, int capacityY)
    {        
        super(2);//means NumberOfDimensions = 2
        this.setCapacity(capacityX, capacityY);
		itemsContained = new Items2D();
    }
	
	public Bin2D()
    {        
        super(2);//means NumberOfDimensions = 2
		itemsContained = new Items2D();		
    }
    
    
    public final void setCapacity (int capacityX, int capacityY)
    {
        ArrayList<Integer> newCapacity = new ArrayList<>();
        newCapacity.add(capacityX);
        newCapacity.add(capacityY);
        super.setCapacity(newCapacity);
    }  
    
    public int getCapacityX()
    {       
        return getCapacity().get(0);
     }
    
    public int getCapacityY()
    {
        return getCapacity().get(1);
    }
	
	public void addItem(Item newItem, int positionX, int positionY)
	{
		ArrayList<Integer> position = new ArrayList<>();
		position.add(positionX);
		position.add(positionY);
		super.addItem(newItem, position);
	}


}
