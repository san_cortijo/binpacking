package upmc.binpacking.bins;

import java.util.ArrayList;

public class Bins1D extends Bins{

	public Bins1D(String binFilePath) 
	{
		super(binFilePath, 1);//1 means 1-D
	}
	
	
	protected final void setCapacityBins (int capacity)
    {
        ArrayList<Integer> newCapacity = new ArrayList<>();
        newCapacity.add(capacity);
        setCapacityBins(newCapacity);
    }      
    
    public int getCapacityX()
    {
        return getCapacityBins().get(0);
    }
	
	@Override
	public Bin instanceBin(ArrayList<Integer> capacity) 
	{
		return new Bin1D(capacity.get(0));
	}
	
}
