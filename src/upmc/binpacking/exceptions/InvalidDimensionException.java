package upmc.binpacking.exceptions;

public class InvalidDimensionException extends Exception{
    
    public InvalidDimensionException(String message)
    {
        super(message);
    }

}
