package upmc.binpacking.exceptions;

public class OverlappingItempsExeption extends Exception {
	
	public OverlappingItempsExeption(String message)
    {
        super(message);
    }
	
}
