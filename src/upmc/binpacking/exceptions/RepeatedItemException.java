package upmc.binpacking.exceptions;


public class RepeatedItemException extends Exception{
    
    public RepeatedItemException(String message)
    {
        super(message);
    }

    
}
