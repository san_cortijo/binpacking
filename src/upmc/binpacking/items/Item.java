package upmc.binpacking.items;

import java.util.ArrayList;
import java.util.Iterator;
import upmc.binpacking.exceptions.InvalidDimensionException;

//TODO : rotation critere (abstract)

public abstract class Item {
	
	private int numberOfDimensions;
    
    private ArrayList<Integer> dimensions;
    
    private String id;   
    
    public Item(String id, int numberOfDimensions)
    {
        dimensions = new ArrayList<>();
        this.id = id;
        this.numberOfDimensions = numberOfDimensions;
    }
    
    public String getID(){
        return id;
    }
    
    private void setID(String newId)
    {
        id = newId;
    }
    
    public void setDimensions(ArrayList<Integer> newDimensions)
    {
        Iterator it = newDimensions.iterator();
        try
        {
            if(newDimensions.size() != numberOfDimensions)
            {
                throw new InvalidDimensionException("Attempt to introduce incorrect number of dimensions to item  " + id );

            } 
            else 
            {           
                for(Integer i : newDimensions)
                {
                    if(i<0)
                    {
                        throw new InvalidDimensionException("Item " + id + " must have POSITIVE dimensions." );
                    }                  
                }
            }
        }   
        catch(Exception e)
        {
                e.printStackTrace();
                //This exit must not be in the GUI version
                System.exit(1);
        }
        dimensions = newDimensions;
    }
    
    public ArrayList<Integer> getDimensions()
    {
        return dimensions;
    }
    
    public int getNumberOfDimensions()
    {
        return numberOfDimensions;
    }
	
	public int getVolumen()
	{
		int volumen=1;
		//System.out.println("DEBUG: getvolumen NbDimensions = " + String.valueOf(numberOfDimensions));
		for(int i=0; i<numberOfDimensions;i++)
		{
			//System.out.println("DEBUG: getvolumen dimension " + String.valueOf(i) + "=" + String.valueOf(getDimensions().get(i)));
			volumen=volumen*getDimensions().get(i);			
		}
		return volumen;
	}


}
