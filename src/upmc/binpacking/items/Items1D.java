package upmc.binpacking.items;

public class Items1D extends Items
{
    
    public Items1D()
    {
        super(1); //1 means 1-Dimension
    }
    
    public Items1D(String itemsFilePath)
    {
        super(1); //1 means 1-Dimension
        readItemsFile(itemsFilePath);
    }
    
    @Override
    public Item1D getItem(int index)
    {
        return Item1D.class.cast(super.getItem(index));
    }
    
    @Override
    public Item instanceItem(String id)
    {
        return new Item1D(0,id);
    }
    

}
