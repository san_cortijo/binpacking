package upmc.binpacking.items;

import java.util.ArrayList;


public class Item1D extends Item 
{
    
    public Item1D(int length, String id) 
    {        
        super(id,1); //1 means 1-Dimentions
        setDimensions(length);
    }
    
  
    public final void setDimensions(int length)
    {
        ArrayList<Integer> newDimentions =  new ArrayList<>();
        newDimentions.add(length);
        setDimensions(newDimentions); 
    }
    
    public int getLength()
    {
        return getDimensions().get(0);
    }  
       
}
