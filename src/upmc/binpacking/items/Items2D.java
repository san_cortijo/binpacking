package upmc.binpacking.items;


public class Items2D extends Items 
{
    
    public Items2D()
    {
        super(2); //2 means 2-Dimensions
    }
    
    public Items2D(String itemsFilePath)
    {
        super(2); //2 means 2-Dimensions
        readItemsFile(itemsFilePath);
    }
    
    	
    @Override
    public Item2D getItem(int index)
    {
        return Item2D.class.cast(super.getItem(index));
    }
    
    @Override
    public Item instanceItem(String id)
    {
        return new Item2D(0,0,id);
    }

    

}
