package upmc.binpacking.items;

import java.util.ArrayList;

public class Item2D extends Item
{
    
    public Item2D(int width, int height, String id) 
    {        
        super(id,2); //2 means 2-Dimentions
        setDimensions(width, height);
    }
    
   
    public final void setDimensions(int width, int height)
    {
        ArrayList<Integer> newDimentions =  new ArrayList<>();
        newDimentions.add(width);
        newDimentions.add(height);
        setDimensions(newDimentions); 
    }
    
    public int getWidth()
    {
        return getDimensions().get(0);
    }
    
    public int getHeight()
    {
        return getDimensions().get(1);
    }
	
	public void rotate()
	{
		setDimensions(getHeight(), getWidth());
	}
	
	public void rotateVertical()
	{
		if(getWidth()>getHeight())
		{
			rotate();
		}
	}
	
	public void rotateHorizontal()
	{
		if(getWidth()<getHeight())
		{
			rotate();
		}
	}

   
}
