package upmc.binpacking.items;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import upmc.binpacking.exceptions.InvalidDimensionException;
import upmc.binpacking.exceptions.RepeatedItemException;


public abstract class Items {
    
    private int numberOfDimensions;

    private ArrayList<Item> items;
    
    private ArrayList<String> itemsIDs;
	
	private static ArrayList<Integer> itemsHash;
	
	private Random randomGenerator;
	
	public Map<String,Integer> itemsMap;

    
    Items(int numberOfDimensions)
    {
        items = new ArrayList<>();
        itemsIDs = new ArrayList<>();
        this.numberOfDimensions = numberOfDimensions;
		itemsMap = new HashMap<>();
		itemsHash = new ArrayList<>();
		randomGenerator = new Random();
    }
    
    public void readItemsFile(String itemsFilePath)
    {
		//TODO: VALIDATE EMPTY LINES
        try (BufferedReader br = new BufferedReader(new FileReader(itemsFilePath)))
		{
			ArrayList<Integer> dimensions;
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) 
			{
				sCurrentLine = sCurrentLine.trim();
				if (!sCurrentLine.startsWith("#") && !sCurrentLine.startsWith("%") && !sCurrentLine.isEmpty())
				{
					String delims = "\\s+";
					String[] values = sCurrentLine.split(delims);
					if (numberOfDimensions+1<values.length)
					{
						System.out.println("WARNING: more dimensions than needed in items file for item ID " + values[0]);
					}
					Item currentItem = instanceItem(values[0]);
					dimensions = new ArrayList<>();
					for(int i=0; i<numberOfDimensions; i++)
					{
						dimensions.add(Integer.parseInt(values[i+1]));
					}
					currentItem.setDimensions(dimensions);
					addItem(currentItem);
				}
			}
 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}  
    }
    

    public int getIndexOfItem(String itemID)
	{
		return itemsIDs.indexOf(itemID);
		//return items.get(numberOfDimensions)
	}
    public void addItem(Item item)
    {
        try
        {
            if(itemsIDs.contains(item.getID()))
            {
                throw new RepeatedItemException("ID "+  item.getID() + " is repeated! Two different items must NOT have the same id");
            }
            else if(item.getNumberOfDimensions()!=numberOfDimensions)
            {
                throw new InvalidDimensionException("Attempt to insert element ID: "+  item.getID() + " which has not proper number of dimensions.");

            }
            else
            {
                items.add(item);
                itemsIDs.add(item.getID());
				int hash = generateNotRepeatedHash();
				//System.out.println("DEBUG: items add hash "+ String.valueOf(hash));
				itemsMap.put(item.getID(),hash);
				//System.out.println(itemsMap.get(item.getID()));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            //Remove this exit in the GUI version
            System.exit(0);
        }
        
    }

    
    public void removeItem(int index)
    {
        items.remove(index);
		itemsIDs.remove(index);
		//TODO: remove values in Hashmap
    }
    
    public Item getItem(int index)
    {
        return items.get(index);
    }
    
    public int getNumberOfItems()
    {
        return items.size();
    }
	
	public int getVolumenOfItems()
	{
		int volumen = 0;
		for(Item item : items)
		{
			volumen = volumen + item.getVolumen();
		}
		return volumen;
	}
    
    public abstract Item instanceItem(String id);
	
	public Integer generateNotRepeatedHash()
	{
		Boolean repeatedValue = true;
		int value = -1;
		while(repeatedValue)
		{
			value = randomGenerator.nextInt();
			if(!itemsHash.contains(value) && value != -1)
			{
				repeatedValue=false;
			}				
		}
		Integer valueInteger = new Integer(value);
		itemsHash.add(new Integer(valueInteger));
		return valueInteger;
	}
	
	public void sortItemsById()
	{
		Collections.sort(items,new ItemsComparator(0));
		itemsIDs.clear();
		for(int i=0;i<getNumberOfItems();i++)
		{
			itemsIDs.add(items.get(i).getID());
		}

	}
	
	public void sortItemsByVolumen()
	{
		Collections.sort(items,new ItemsComparator(1));	
		itemsIDs.clear();
		for(int i=0;i<getNumberOfItems();i++)
		{
			itemsIDs.add(items.get(i).getID());
		}
	}
	
	public void sortItemsByVolumenDesc()
	{
		Collections.sort(items,new ItemsComparator(2));	
		itemsIDs.clear();
		for(int i=0;i<getNumberOfItems();i++)
		{
			itemsIDs.add(items.get(i).getID());
		}
	}
	
	public void sortItemsByDimension(int dim)
	{
		Collections.sort(items,new ItemsComparator(3,dim));
		for(int i=0;i<getNumberOfItems();i++)
		{
			itemsIDs.add(items.get(i).getID());
		}
	}
	
	
	public void sortItemsByDimensionDesc(int dim)
	{
		Collections.sort(items,new ItemsComparator(4,dim));
		for(int i=0;i<getNumberOfItems();i++)
		{
			itemsIDs.add(items.get(i).getID());
		}
	}
	
	public void sortItemsByWidth()
	{
		if(numberOfDimensions==2)
		sortItemsByDimension(1);
	}
	
	public void sortItemsByHeight()
	{
		if(numberOfDimensions==2)
		sortItemsByDimension(2);
	}
	
	public void sortItemsByWidthDesc()
	{
		if(numberOfDimensions==2)
		sortItemsByDimensionDesc(1);
	}
	
	public void sortItemsByHeightDesc()
	{
		if(numberOfDimensions==2)
		sortItemsByDimensionDesc(2);
	}
        
        
    

}
