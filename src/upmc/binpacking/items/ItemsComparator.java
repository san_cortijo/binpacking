package upmc.binpacking.items;

import java.util.Comparator;

public class ItemsComparator implements Comparator<Item>

{
	public static final int COMPARE_BY_ID = 0;
	public static final int COMPARE_BY_VOLUMEN = 1;
	public static final int COMPARE_BY_VOLUMEN_DESC = 2;
	public static final int COMPARE_BY_N_DIMENSION = 3;
	public static final int COMPARE_BY_N_DIMENSION_DESC = 4;
	
	private int compareMode;
	private int dimensionToCompare;
	
	public ItemsComparator(int compareMode) 
	{
		this.compareMode = compareMode;
		dimensionToCompare = 0;

	}
	
	public ItemsComparator(int compareMode, int dimensionToCompare) 
	{
		this.compareMode = compareMode;
		this.dimensionToCompare = dimensionToCompare;
	}
	
	
	@Override
	public int compare(Item o1, Item o2) 
	{
		Integer i1;
		Integer i2;

		switch(compareMode)
		{
			case COMPARE_BY_ID:
				return o1.getID().compareTo(o2.getID());				
			case COMPARE_BY_VOLUMEN:
				i1 = new Integer(o1.getVolumen());
				i2 = new Integer(o2.getVolumen());
				return i1.compareTo(i2);
			case COMPARE_BY_VOLUMEN_DESC:
				i1 = new Integer(o1.getVolumen());
				i2 = new Integer(o2.getVolumen());	
				return i2.compareTo(i1);
			case COMPARE_BY_N_DIMENSION:
				i1 = new Integer(o1.getDimensions().get(dimensionToCompare));
				i2 = new Integer(o2.getDimensions().get(dimensionToCompare));
				return i1.compareTo(i2);	
			case COMPARE_BY_N_DIMENSION_DESC:
				i1 = new Integer(o1.getDimensions().get(dimensionToCompare));
				i2 = new Integer(o2.getDimensions().get(dimensionToCompare));
				return i2.compareTo(i1);	
			default:
				throw new UnsupportedOperationException("Not supported Comparison.");
		}
			
			
	}
	
	

	
}
