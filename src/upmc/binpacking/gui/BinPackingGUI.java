package upmc.binpacking.gui;

import upmc.binpacking.main.BinPacking;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JMenuBar;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;



public class BinPackingGUI extends JPanel implements ActionListener, ItemListener {
	
	
	private static final long serialVersionUID = 758753494188210988L;
	
	JSplitPane splitPane;	
	
    JTabbedPane tabbedPane;
	
	protected JTextArea textArea;
	
	
	static private JFrame frame;
	
	
    JScrollPane scrollPane;
	
	
	//TODO: Boolean reportReady for enabling saving
    JButton openItemsFileButton; 
	JButton openBinFileButton;
	JButton saveButton;
	JButton saveAllButton;
	JButton runButton;
	JButton infoButton;
	JButton resetButton;
	JMenuItem aboutButton;
    //JTextArea log = new JTextArea();
    JFileChooser fc;
	
	JTable table;
	
	JRadioButtonMenuItem button1DFF;
	JRadioButtonMenuItem button1DBF;
	JRadioButtonMenuItem button1DFFD;
	JRadioButtonMenuItem button1DBFD;
	JRadioButtonMenuItem button1DNF;
	JRadioButtonMenuItem button1DNFD;


	
	JRadioButtonMenuItem button2DFF;
	JRadioButtonMenuItem button2DBF;
	JRadioButtonMenuItem button2DFFD;
	JRadioButtonMenuItem button2DBFD;
	JRadioButtonMenuItem button2DNF;
	JRadioButtonMenuItem button2DNFD;

	
	JCheckBoxMenuItem useRotationCheckButton;
	
	JMenu editMenu;

	
	JMenuItem runMenuButton;
	
	JTextArea reportBP;
	
	Boolean binLoaded = false;
	Boolean itemsLoaded = false;
	
	
	DrawPanel gReportPanne;
	
	JLabel bottomLabel;


	private static int numberOfDimensions;
	
	private static String method;
	
	private static BinPacking binPacking;
	
	private static String report;
	
	void instanceThreads(){
		
			runThread = new Thread(){
			@Override
			public void run(){
				//TODO: Put "Please wait signal here"

				binPacking.runBinPacking(numberOfDimensions, method);

				if(numberOfDimensions == 1)
				{
					report = binPacking.generateReport1D();
					createReportTab(report);
				} else
				{
					report = binPacking.generateReport2D();
					createReportTab(report);
				}

				createGraphicReportTab();
				
				saveButton.setEnabled(true);
				
				bottomLabel.removeAll();
				bottomLabel.repaint();
				splitPane.remove(bottomLabel);
				createBottomPanel();
				

			}
		};

		loadItemsThread = new Thread(){
			@Override
			public void run(){
			System.out.println("Inside load thread");
			System.out.println(binPacking.itemsFilePath);
			loadItems();
			//System.out.println(binPacking.itemsFilePath);
			createItemsTab(binPacking.generateItemsTable(numberOfDimensions));

			}
		};

		loadBinThread = new Thread(){
		@Override
		public void run(){
		loadBin();
		System.out.println("BIN LOADED");
		createBinTab(binPacking.generateBinTable());
			}
		};
	
	}
	
	Thread runThread = new Thread(){
		@Override
		public void run(){
			//TODO: Put "Please wait signal here"
			
			
			binPacking.runBinPacking(numberOfDimensions, method);
			
			if(numberOfDimensions == 1)
			{
				report = binPacking.generateReport1D();
				createReportTab(report);
			} else
			{
				report = binPacking.generateReport2D();
				createReportTab(report);
			}
			
			
			createGraphicReportTab();
			saveButton.setEnabled(true);
			
			/*bottomLabel.removeAll();
			bottomLabel.repaint();
			
			splitPane.remove(bottomLabel);
			createBottomPanel();*/


		}
	};
	
	Thread loadItemsThread = new Thread(){
		@Override
		public void run(){
		System.out.println("Inside load thread");
		System.out.println(binPacking.itemsFilePath);
		loadItems();
		//System.out.println(binPacking.itemsFilePath);
		createItemsTab(binPacking.generateItemsTable(numberOfDimensions));
		
		}
	};
	
	Thread loadBinThread = new Thread(){
	@Override
	public void run(){
	loadBin();
	System.out.println("BIN LOADED");
	createBinTab(binPacking.generateBinTable());
		}
	};
    

    public BinPackingGUI()
    {    	

    	super(new BorderLayout());
        fc = new JFileChooser();
        tabbedPane = new JTabbedPane();
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        createToolBar();
    	createMiddlePanels(); 
    	createTabbedPanel();
    	createBottomPanel();
		
		
    }
	
	static private void instanceBinPacking()
	{
		binPacking = new BinPacking();
	}

	
	private void loadItems()
	{
		if(numberOfDimensions ==  1)
		{
			binPacking.numberOfDimensions = 1;
			binPacking.load1dItems();
		} else if (numberOfDimensions == 2)
		{
			binPacking.numberOfDimensions = 2;
			binPacking.load2DItems();
		}
	}
	
	private void loadBin()
	{
	
		if(numberOfDimensions ==  1)
		{
			binPacking.numberOfDimensions = 1;
			binPacking.load1dBin();
		} else if (numberOfDimensions == 2)
		{
			binPacking.numberOfDimensions = 2;
			binPacking.load2DBin();
		}

	}

	
    public void createBottomPanel()
    {
    	bottomLabel = new JLabel("BIN PACKING.  S. CORTIJO, J.HAMMOUD.  \tPIAD-UPMC \u00a9 2014", JLabel.LEFT);
    	add(bottomLabel, BorderLayout.PAGE_END);
    }
	
	
	public void createLoadingMessage()
	{
		String imageName = "wait";
		String imgLocation = "images/" + imageName + ".gif";
		URL url = BinPackingGUI.class.getResource(imgLocation);
		Icon icon = new ImageIcon(url);
		JOptionPane.showMessageDialog(frame,"Generating bin packing reports. This may take a minut. Please wait!",	"Generating report. Please wait!",	JOptionPane.INFORMATION_MESSAGE,icon);
	}
    
    public void createTabbedPanel() {
        
        ImageIcon icon = createImageIcon("images/middle.gif");
        splitPane.setRightComponent(tabbedPane);        
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    }
	
	public void createItemsTab(Object[][] data)
	{
		
		if(numberOfDimensions == 2)
		{
			String[] columnNames = {"Item ID",
									"Dim X",
									"Dim Y"};

			table = new JTable(data, columnNames);
			table.setPreferredScrollableViewportSize(new Dimension(500, 70));
			table.setFillsViewportHeight(true);


			//Create the scroll pane and add the table to it.
			JScrollPane scrPanne = new JScrollPane(table);

			//Add the scroll pane to this panel.
			ImageIcon icon = createImageIcon("images/middle.gif");
			tabbedPane.addTab("Items", icon, scrPanne, "Items Dimensions");
		} else 
		{
			String[] columnNames = {"Item ID",
                                "Length"};

			table = new JTable(data, columnNames);
			table.setPreferredScrollableViewportSize(new Dimension(500, 70));
			table.setFillsViewportHeight(true);


			//Create the scroll pane and add the table to it.
			JScrollPane scrPanne = new JScrollPane(table);

			//Add the scroll pane to this panel.
			ImageIcon icon = createImageIcon("images/middle.gif");
			tabbedPane.addTab("Items", icon, scrPanne, "Items Dimensions");
			
			
		}
	

		
		
	}
	
	public void createBinTab(Object[][] data)
	{
		if(numberOfDimensions == 2)
		{
			String[] columnNames = {"Capacity X",
									"Capacity Y"};

			table = new JTable(data, columnNames);
			table.setPreferredScrollableViewportSize(new Dimension(500, 70));
			table.setFillsViewportHeight(true);


			//Create the scroll pane and add the table to it.
			JScrollPane scrPanne = new JScrollPane(table);

			//Add the scroll pane to this panel.
			ImageIcon icon = createImageIcon("images/middle.gif");
			tabbedPane.addTab("Bin", icon, scrPanne, "Bin Dimensions");
		} else
		{
			String[] columnNames = {"Capacity "};

			table = new JTable(data, columnNames);
			table.setPreferredScrollableViewportSize(new Dimension(500, 70));
			table.setFillsViewportHeight(true);


			//Create the scroll pane and add the table to it.
			JScrollPane scrPanne = new JScrollPane(table);

			//Add the scroll pane to this panel.
			ImageIcon icon = createImageIcon("images/middle.gif");
			tabbedPane.addTab("Bin", icon, scrPanne, "Bin Dimensions");
		}
	
	}
	
	public void createReportTab(String report)
	{
		reportBP = new JTextArea(report);
		JScrollPane scrPanne = new JScrollPane(reportBP);
		
		 //Add the scroll pane to this panel.
        ImageIcon icon = createImageIcon("images/middle.gif");
		//tabbedPane.insertTab("Report", icon, scrPanne, "Repor Bin Packing",0);
		tabbedPane.addTab("Report", icon, scrPanne, "Report Bin Packing");

		
	}
	
	public void createGraphicReportTab()
	{
		if(numberOfDimensions == 2)
		{
			gReportPanne = new DrawPanel(BinPacking.binsDistribution());
			JScrollPane scrPanne = new JScrollPane(gReportPanne);
			ImageIcon icon = createImageIcon("images/middle.gif");
			scrPanne.setPreferredSize(gReportPanne.getSize());
			tabbedPane.addTab("Graphical Report", icon, scrPanne, "Report Bin Packing");
		}
		

	}
	
    public void createMiddlePanels()
    {
    	
        splitPane.setDividerLocation(200);         
        splitPane.setPreferredSize(new Dimension(500, 300));
        add(splitPane, BorderLayout.CENTER);
		
    }
    
    
    
    public void createToolBar() {
        //Create the toolbar.
        JToolBar toolBar = new JToolBar("ToolBar");
        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        openItemsFileButton = makeNavigationButton("Open24", "Open Items File", "Open Items File","Open Items File");        
        toolBar.add(openItemsFileButton);
		
		openBinFileButton = makeNavigationButton("J2EEServer24", "Open Bin File", "Open Bin File","Open Bin File");        
        toolBar.add(openBinFileButton);
        
        saveButton = makeNavigationButton("Save24", "Save", "Save Report","Save");
		saveButton.setEnabled(false);
        toolBar.add(saveButton);
				
        runButton = makeNavigationButton("Host24", "Process Bin Packing", "Process Bin Packing","Process Bin Packing");
		runButton.setEnabled(false);
		toolBar.add(runButton);
		
        toolBar.addSeparator();
		
		resetButton = makeNavigationButton("Stop24", "Reset", "Reset","Reset");
		toolBar.add(resetButton);

		 
		infoButton = makeNavigationButton("Information24", "Help Content", "Help Content", "Help Content");
		toolBar.add(infoButton);
        add(toolBar, BorderLayout.PAGE_START);
        
    }
    

    public JMenuBar createMenuBar() {
        JMenuBar menuBar;
        JMenu fileMenu;

        //Create the menu bar.
        menuBar = new JMenuBar();

        //Build the first menu.
        fileMenu = new JMenu("File");
        //fileMenu.setMnemonic(KeyEvent.VK_A);
        fileMenu.getAccessibleContext().setAccessibleDescription(
                "File menu");
        menuBar.add(fileMenu);

        //a group of JMenuItems
		/*
        menuItem = new JMenuItem("New Project", KeyEvent.VK_T);
        //menuItem.setMnemonic(KeyEvent.VK_T); //used constructor instead
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("OpenFile Items File");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
		*/

        ImageIcon icon = createImageIcon("images/middle.gif");
		/*
        menuItem = new JMenuItem("Load Project", icon);
        menuItem.setMnemonic(KeyEvent.VK_B);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
		*/
		/*
        menuItem = new JMenuItem("Save Project", icon);
        menuItem.setMnemonic(KeyEvent.VK_D);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
		*/

        fileMenu.addSeparator();
        ButtonGroup group = new ButtonGroup();
        /*
        menuItem = new JMenuItem("Open Items file...");
        menuItem.setMnemonic(KeyEvent.VK_D);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);   
		*/
        /*
        menuItem = new JMenuItem("Save report as...");
        menuItem.setMnemonic(KeyEvent.VK_D);
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
		*/
        
        //fileMenu.addSeparator();
        //menuItem = new JMenuItem("Exit");
        //fileMenu.add(menuItem);     
		
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		fileMenu.add(exit);
       
        //Build second menu in the menu bar.
		
		
        editMenu = new JMenu("Edit");
        editMenu.getAccessibleContext().setAccessibleDescription("Edit Menu");
        
        /*runMenuButton = new JMenuItem("Process Bin Packing");
        runMenuButton.addActionListener(this);*/
        //editMenu.add(runMenuButton);
		

		button1DNF = new JRadioButtonMenuItem("1D : NEXT FIT");
        button1DNF.setSelected(true);
        group.add(button1DNF);
        button1DNF.addActionListener(this);
        editMenu.add(button1DNF);
		
		button1DNFD = new JRadioButtonMenuItem("1D : NEXT FIT DECREASING");
        group.add(button1DNFD);
        button1DNFD.addActionListener(this);
        editMenu.add(button1DNFD);
		
        button1DFF = new JRadioButtonMenuItem("1D : FIRST FIT");
        group.add(button1DFF);
        button1DFF.addActionListener(this);
        editMenu.add(button1DFF);
        
        button1DFFD = new JRadioButtonMenuItem("1D : FIRST FIT DECREASING");
        group.add(button1DFFD);
        button1DFFD.addActionListener(this);
        editMenu.add(button1DFFD);
		
        button1DBF = new JRadioButtonMenuItem("1D : BEST FIT");
        group.add(button1DBF);
        button1DBF.addActionListener(this);
        editMenu.add(button1DBF);
        
        button1DBFD = new JRadioButtonMenuItem("1D : BEST FIT DECREASING");
        group.add(button1DBFD);
        button1DBFD.addActionListener(this);
        editMenu.add(button1DBFD);
        
        editMenu.addSeparator();
		
		button2DNF = new JRadioButtonMenuItem("2D : NEXT FIT");
        group.add(button2DNF);
        button2DNF.addActionListener(this);
        editMenu.add(button2DNF); 
		
		button2DNFD = new JRadioButtonMenuItem("2D : NEXT FIT DECREASING");
        group.add(button2DNFD);
        button2DNFD.addActionListener(this);
        editMenu.add(button2DNFD); 

        
        button2DFF = new JRadioButtonMenuItem("2D : FIRST FIT");
        group.add(button2DFF);
        button2DFF.addActionListener(this);
        editMenu.add(button2DFF);  
		
		button2DFFD = new JRadioButtonMenuItem("2D : FIRST FIT DECREASING");
        group.add(button2DFFD);
        button2DFFD.addActionListener(this);
        editMenu.add(button2DFFD);
		

        button2DBF = new JRadioButtonMenuItem("2D : BEST FIT");
        group.add(button2DBF);
        button2DBF.addActionListener(this);
        editMenu.add(button2DBF);
        
        button2DBFD = new JRadioButtonMenuItem("2D : BEST FIT DECREASING");
        group.add(button2DBFD);
        button2DBFD.addActionListener(this);
        editMenu.add(button2DBFD);
		
		editMenu.addSeparator();
		
		useRotationCheckButton = new JCheckBoxMenuItem("Allow Rotation");
        group.add(useRotationCheckButton);
        useRotationCheckButton.addActionListener(this);
        editMenu.add(useRotationCheckButton);

        
        menuBar.add(editMenu);        
        
        
        fileMenu = new JMenu("Help");
        fileMenu.getAccessibleContext().setAccessibleDescription(
                "File menu");
        menuBar.add(fileMenu);
        
        //a group of JMenuItems
        /*menuItem = new JMenuItem("Content", icon);
        menuItem.setMnemonic(KeyEvent.VK_T); //used constructor instead
        //menuItem.setAccelerator(KeyStroke.getKeyStroke( KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("Help Content");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);
        fileMenu.addSeparator();*/

        
        aboutButton = new JMenuItem("About");
        aboutButton.getAccessibleContext().setAccessibleDescription("About");
        aboutButton.addActionListener(this);
        fileMenu.add(aboutButton);  
       
        return menuBar;
    }


	@Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == openItemsFileButton) {
            int returnVal = fc.showOpenDialog(BinPackingGUI.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
				binPacking.itemsFilePath = file.toString();
				loadItemsThread.start();
				openItemsFileButton.setEnabled(false);
				editMenu.setEnabled(false);
				itemsLoaded = true;
				if(itemsLoaded && binLoaded)
				{
					runButton.setEnabled(true);
				}
            } else {
            }

        } else if  (e.getSource() == openBinFileButton) {
			int returnVal = fc.showOpenDialog(BinPackingGUI.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
				binPacking.binFilePath = file.toString();
				loadBinThread.start();
				openBinFileButton.setEnabled(false);
				editMenu.setEnabled(false);
				binLoaded = true;
				if(itemsLoaded && binLoaded)
				{
					runButton.setEnabled(true);
				}
				
            } else {
            }

        } else if (e.getSource() == saveButton) {
            int returnVal = fc.showSaveDialog(BinPackingGUI.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
				try 
				{
					System.out.println("file saved to "+file.getAbsolutePath());
					PrintWriter out;
					out = new PrintWriter(file.getAbsolutePath());
					System.out.println(report);
					out.println(report);
					out.close();
				}
				catch (FileNotFoundException ex) 
				{
					ex.printStackTrace();
				}
			
				
            } 	else {
            }
        } else if (e.getSource() == runButton || e.getSource()== runMenuButton){
				runButton.setEnabled(false);
				createLoadingMessage();
				runThread.start();
				
			
		} else if (e.getSource() == button1DFF)
		{
			numberOfDimensions = 1;
			binPacking.numberOfDimensions = numberOfDimensions;
			method = "firstFit1D";
			System.out.println(method);
		} else if (e.getSource() == button1DFFD)
		{
			numberOfDimensions = 1;
			method = "firstFitDesc1D";
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		}else if (e.getSource() == button1DNF)
		{
			method = "nextFit1D";
			numberOfDimensions = 1;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		} else if (e.getSource() == button1DNFD)
		{
			method = "nextFitDesc1D";
			numberOfDimensions = 1;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		}else if (e.getSource() == button1DBF)
		{
			numberOfDimensions = 1;
			method = "bestFit1D";
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;

		} else if (e.getSource() == button1DBFD)
		{
			numberOfDimensions = 1;
			method = "bestFitDesc1D";
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		} else if (e.getSource() == button2DFF)
		{
			method = "firstFit2D";
			numberOfDimensions = 2;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		} else if (e.getSource() == button2DFFD)
		{
			method = "firstFitDesc2D";
			numberOfDimensions = 2;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		} else if (e.getSource() == button2DNF)
		{
			method = "nextFit2D";
			numberOfDimensions = 2;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		}  else if (e.getSource() == button2DNFD)
		{
			method = "nextFitDesc2D";
			numberOfDimensions = 2;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		}else if (e.getSource() == button2DBF)
		{
			method = "bestFit2D";
			numberOfDimensions = 2;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		} else if (e.getSource() == button2DBFD)
		{
			method = "bestFitDesc2D";
			numberOfDimensions = 2;
			System.out.println(method);
			binPacking.numberOfDimensions = numberOfDimensions;
		} else if (e.getSource() == aboutButton || e.getSource() == infoButton)
		{			
			String imageName = "upmcSmall";
			String imgLocation = "images/" + imageName + ".gif";
			URL url = BinPackingGUI.class.getResource(imgLocation);
			Icon icon = new ImageIcon(url);
			
			JOptionPane.showMessageDialog(frame,"BIN PACKING \nAdvisor:\n\tT. LUST\nGroup:\n\tS. CORTIJO\n\tJ.HAMMOUD\nPIAD-UPMC \u00a9 2014",	"UPMC 2014 - PIAD 26",	JOptionPane.INFORMATION_MESSAGE,icon);
		} else if (e.getSource() == resetButton)
		{
			itemsLoaded = false;
			binLoaded = false;
			editMenu.setEnabled(true);
			runButton.setEnabled(false);
			saveButton.setEnabled(false);
			openItemsFileButton.setEnabled(true);
			openBinFileButton.setEnabled(true);
			tabbedPane.removeAll();
			instanceBinPacking();
			binPacking.rotation = useRotationCheckButton.isSelected();
			instanceThreads();
		} else if (e.getSource() == useRotationCheckButton)
		{
			binPacking.rotation = useRotationCheckButton.isSelected();
		}
        
    }
	

    @Override
    public void itemStateChanged(ItemEvent e) {
 
    }


    /** Returns an ImageIcon, or null if the path was invalid.
	 * @return  */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = BinPackingGUI.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        frame = new JFrame("Bin Packing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BinPackingGUI demo = new BinPackingGUI();
        frame.setJMenuBar(demo.createMenuBar());
        frame.setContentPane(demo);
        frame.setVisible(true);        
        frame.getContentPane().setPreferredSize( Toolkit.getDefaultToolkit().getScreenSize());
        frame.pack();
    }
    
    protected JButton makeNavigationButton(String imageName,
		            String actionCommand,
		            String toolTipText,
		            String altText) {
		//Look for the image.
		String imgLocation = "images/"
		+ imageName
		+ ".gif";
		URL imageURL = BinPackingGUI.class.getResource(imgLocation);
		
		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageURL != null) {//image found
		button.setIcon(new ImageIcon(imageURL, altText));
		} else {//no image found
		button.setText(altText);
		System.err.println("Resource not found: "
		+ imgLocation);
		}
		
		return button;
	}
		
		
    public static void main(String[] args) {
		numberOfDimensions = 1;
		method = "nextFit1D";
		instanceBinPacking();
		
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
    
}
