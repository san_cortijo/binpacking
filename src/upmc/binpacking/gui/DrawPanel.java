package upmc.binpacking.gui;

/**
 *
 * @author santiago
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
//import java.awt.Rectangle;
import javax.swing.JPanel;
import upmc.binpacking.bins.Bin;

import upmc.binpacking.bins.Bins;

public class DrawPanel extends JPanel
{

	public int panelWidth = 800;
	public int panelHeight = 800;
	
	public int scale = 2;
	
	public int nBins;
	private Bins bins;

	public DrawPanel(Bins b)
	{
		setBackground( Color.WHITE );
		bins = b;
		nBins = bins.getNumberOfBins();


	} 

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		drawBins(g);

	}

	private void drawBins(Graphics g)
	{
		int nCols = 3;
		int row;
		int col;
		

		int w = bins.getCapacityBins().get(0);
		int h = bins.getCapacityBins().get(1);
		int sY= (int)(w*1.2);
		int sX = (int)(h*1.2);
		
		w = w*scale;
		h = h*scale;
		sX = sX*scale;
		sY = sY*scale;
		for(int bin=0;bin<nBins;bin++)
		{
		   row = bin%nCols;
		   col = bin/nCols;
		   int binX = 50+sX*row;
		   int binY = 50+sY*col;
		   
		   g.drawRect(binX, binY, h, w);
		   g.drawString("Bin " + (bin+1), binX + h/2 - 20 , binY + w + 20);
		   
		   drawItems( g, binX,binY , bin);

		}
		 
		 panelWidth = (nCols+1)*sX + 100;
		 panelHeight = (nBins/4 +1)*sY+ 200;
	}
	
	private void drawItems(Graphics g, int binX, int binY,int bin )
	{
		
		String currentID;
		Color c;
		ArrayList<Color> colors = new ArrayList<>();
		
		colors.add(Color.red);
		colors.add(Color.blue);
		colors.add(Color.MAGENTA);
		colors.add(Color.CYAN);
		colors.add(Color.orange);
		colors.add(Color.PINK);
		
		int ncolors = colors.size();
		
		Random rand = new Random();

		//int  n = rand.nextInt(50) + 1;

		for(int item=0;item<bins.getBin(bin).getNumberOfItemsContained();item++)
		{
			currentID = bins.getBin(bin).getItem(item).getID();
			int w = bins.getBin(bin).getItem(item).getDimensions().get(0);
			int h = bins.getBin(bin).getItem(item).getDimensions().get(1);
			w = w*scale;
			h = h*scale;
			int posY = Bin.itemsPositions.get(bins.getBin(bin).getItem(item).getID()).get(0);
			int posX = Bin.itemsPositions.get(bins.getBin(bin).getItem(item).getID()).get(1);
			posX = posX*scale;
			posY = posY*scale;
			posX = posX + binX;
			posY = posY + binY; 
			//g.setColor(colors.get(rand.nextInt(ncolors)));
			g.setColor(colors.get(item%ncolors));
			g.fillRect(posX, posY, h, w);
			g.setColor(Color.BLACK);
			g.drawRect(posX, posY, h, w);
			g.drawString(currentID, posX + h/2 - 20   , posY + w/2 + 10 );


		}
	}
   
	@Override
	public Dimension getPreferredSize() {
		 // The parent scrollPane will ask for this
		 // and use it to set its scrollbars to
		 // make this component fully visible.
		 return new Dimension(panelWidth, panelHeight);
	 }
}
