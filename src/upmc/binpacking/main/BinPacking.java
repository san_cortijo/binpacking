package upmc.binpacking.main;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import upmc.binpacking.bins.Bin;
import upmc.binpacking.bins.Bin1D;
import upmc.binpacking.bins.Bin2D;
import upmc.binpacking.bins.Bins;
import upmc.binpacking.bins.Bins1D;
import upmc.binpacking.bins.Bins2D;
import upmc.binpacking.items.Item1D;
import upmc.binpacking.items.Item2D;
import upmc.binpacking.items.Items;
import upmc.binpacking.items.Items1D;
import upmc.binpacking.items.Items2D;
/**
 * BinPacking is the core class in which many bin-packing algorithms are code by means of the classes {@link Bins} , {@link Items} 
 * and their extentions.
 * 
 * @author Santiago Cortijo
 * @author Jamal Hammoud
 */
public class BinPacking {
	
	
	public void runBinPacking(Integer nDimensions, String method)
	{
		if(nDimensions == 1)
		{
			switch (method) {
				case "firstFit1D":
					firstFit1D();
					System.out.println("Running " + method);
					break;
				case "bestFit1D":
					bestFit1D();
					System.out.println("Running " + method);
					break;
				case "nextFit1D":
					nextFit1D();
					System.out.println("Running " + method);
					break;
				case "firstFitDesc1D":
					firstFitDecreasing1D();
					System.out.println("Running " + method);
					break;
				case "bestFitDesc1D":
					bestFitDecreasing1D();
					System.out.println("Running " + method);
					break;
				case "nextFitDesc1D":
					nextFitDecreasing1D();
					System.out.println("Running " + method);
					break;
			}
			
		}
		else if (nDimensions == 2)
		{
			switch (method) {
				case "firstFit2D":
					System.out.println("Running " + method);
					firsFit2D();
					break;
				case "bestFit2D":
					bestFit2D();
					System.out.println("Running " + method);
					break;
				case "nextFit2D":
					nextFit2D();
					System.out.println("Running " + method);
					break;
				case "firstFitDesc2D":
					firstFitDecreasing2D();
					System.out.println("Running " + method);
					break;
				case "bestFitDesc2D":
					bestFitDecreasing2D();
					System.out.println("Running " + method);
					break;
				case "nextFitDesc2D":
					nextFitDecreasing2D();
					System.out.println("Running " + method);
					break;
			}
		}
	}
    
    private static Bins bins;    
    
    private static Items items;
    
    public static String binFilePath;
    
    public static String itemsFilePath;
	
	private static String reportFilePath;
	
	public static int numberOfDimensions;
	
	private static String methodUsed;
	
	public static Boolean rotation = false;
	
	public static void load1dData()
	{
		//Bin 1D Example
		numberOfDimensions = 1; //Temporal
		String message;
		bins = new Bins1D(binFilePath);
		message = "Loaded Bin Dimensions :\n";
		message += "Length = "+ Bins1D.class.cast(bins).getCapacityX();
		System.out.println(message);
		items = new Items1D(itemsFilePath);
		System.out.println();
		int nitems = items.getNumberOfItems();        
		System.out.println("Loaded " + String.valueOf(nitems) + " items:");
		for (int i = 0; i<nitems ; i++)
		{   
			message = "\tElement " + String.valueOf(i) + ": ";
			message = message + "id=" +items.getItem(i).getID() + " ";
			message = message + "L=" + String.valueOf(Item1D.class.cast(items.getItem(i)).getLength()) + " ";
			//System.out.println(message);            
		}
	}
	
	public static void load1dItems()
	{
		//This function is meant to be called by de GUI
		items = new Items1D(itemsFilePath);
	}
	
	public static void load1dBin()
	{
		//This function is meant to be called by de GUI
		bins = new Bins1D(binFilePath);
	}
	
	public static void load2DItems()
	{
		//This function is meant to be called by de GUI
		items = new Items2D(itemsFilePath);
	}
	
	public static void load2DBin()
	{		
		//This function is meant to be called by de GUI
		bins =  new Bins2D(binFilePath);
	}
	
	public static void load2dData()
	{
		numberOfDimensions = 2; //Temporal
		String message;
		bins =  new Bins2D(binFilePath);
		message = "Loaded Bin :\n";
		message = message + "\tCx = " + String.valueOf(Bins2D.class.cast(bins).getCapacityX());
		message = message + "\tCy = " + String.valueOf(Bins2D.class.cast(bins).getCapacityY());

		System.out.println(message);

		items = new Items2D(itemsFilePath);
		System.out.println();
		int nitems = items.getNumberOfItems();        
		System.out.println("Loaded " + String.valueOf(nitems) + " items:");
		for (int i = 0; i<nitems ; i++)
		{   
			message = "\tElement " + String.valueOf(i) + ": ";
			message = message + "id=" +items.getItem(i).getID() + " ";
			message = message + "w=" + String.valueOf(Items2D.class.cast(items).getItem(i).getWidth()) + " ";
			message = message + "h=" + String.valueOf(Items2D.class.cast(items).getItem(i).getHeight());
			//System.out.println(message);            
		}
	}
	
	public static Object[][] generateItemsTable(int numberOfDimensions)
	{
		Object[][] itemsTable;
		int nitems = items.getNumberOfItems(); 
		if(numberOfDimensions == 1)
		{
			itemsTable =  new Object[nitems][2];
			/*itemsTable[0][0] = "ItemID";
			itemsTable[0][1] = "Length";*/
			
			for (int i = 0; i<nitems ; i++)
			{   
				itemsTable[i][0] = items.getItem(i).getID();
				itemsTable[i][1] =  String.valueOf(Item1D.class.cast(items.getItem(i)).getLength());
			}
		
		} else {
			
			itemsTable =  new Object[nitems][3];
			/*
			itemsTable[0][0] = "ItemID";
			itemsTable[0][1] = "LengthX";
			itemsTable[0][2] = "LengthY";*/
			
			for (int i = 0; i<nitems ; i++)
			{   
				itemsTable[i][0] = items.getItem(i).getID();
				itemsTable[i][1] = String.valueOf(Items2D.class.cast(items).getItem(i).getWidth());
				itemsTable[i][2] = String.valueOf(Items2D.class.cast(items).getItem(i).getHeight());
			}
		
		}
		
		return itemsTable;
	}
	
	public static Object[][] generateBinTable()
	{
		Object[][] binTable;
		
		//binTable = new Object[1][numberOfDimensions];
		
		
		if(numberOfDimensions == 1)
		{
			binTable = new Object[1][1];
			binTable[0][0] = String.valueOf(Bins1D.class.cast(bins).getCapacityX());
		} else
		{
			binTable = new Object[1][2];
			binTable[0][0] = String.valueOf(Bins2D.class.cast(bins).getCapacityX());
			binTable[0][1] = String.valueOf(Bins2D.class.cast(bins).getCapacityY());
		}
		
		
		return binTable;
	}
	
	private static void bins1DRoutine()
	{
		//Bin 1D example
		methodUsed = "Example 1D Routine";
		load1dData();
		//Adding 3 bins
		bins.addNewBin();
		bins.addNewBin();
		bins.addNewBin();
		
		//Adding item 1 m to  bin1, to position 100
		Bin1D.class.cast(bins.getBin(1)).addItem(items.getItem(0), 100);
		//Adding item 1 m to  bin0, to position 111 
		Bin1D.class.cast(bins.getBin(0)).addItem(items.getItem(1), 111);
		//Adding item 3 m to  bin0, to position 0
		Bin1D.class.cast(bins.getBin(0)).addItem(items.getItem(2), 0);
		
	}
	
	private static void bins2DRoutine()
	{			
		//Bin 2D example
		methodUsed = "Example 2D Routine";
		//Adding 3 bins
		bins.addNewBin();
		bins.addNewBin();
		bins.addNewBin();

		//Adding item 0 m to  bin0, to position 10 , 10
		Bin2D.class.cast(bins.getBin(0)).addItem(items.getItem(0),10,10);
		//Adding item 1 m to  bin2, to position 40 , 49
		Bin2D.class.cast(bins.getBin(2)).addItem(items.getItem(1),40,49);
		//Adding item 3 m to  bin2, to position 0 , 10
		Bin2D.class.cast(bins.getBin(2)).addItem(items.getItem(2),0,10);
		
	}
            
    
	

	
	public static Bins binsDistribution()
	{
		return bins;
	}
	
	public static void firstFit1D()
	{
		methodUsed = "First Fit 1D";
		int capacity =  Bins1D.class.cast(bins).getCapacityX();
		bins.addNewBin();
		Bin1D binTmp;
		Item1D itemTmp;
		int nBins = 1;
		int nItems = items.getNumberOfItems();
		itemsLoop:
		for(int i=0; i<nItems;)
		{
			itemTmp = Item1D.class.cast(items.getItem(i));
			if(itemTmp.getLength()>capacity)
			{
				//TODO: Drop exception here
				i++;
				continue ;
			}
			
			Boolean newBin;
			newBin = true;
			
			binsLoop:
			for(int bin=0;bin<nBins;bin++)
			{
				binTmp = Bin1D.class.cast(bins.getBin(bin));

				if(itemTmp.getVolumen() > (1-binTmp.getOccupancy())*binTmp.getVolumen())
				{
					// Don't even try to search if not enough space in the bin
					continue;
				}
				for(int pos=0;pos<capacity;pos++)
				{	
					if(binTmp.isFreePosition(itemTmp, pos))
					{
						binTmp.addItem(itemTmp, pos);
						newBin=false;
						i++;
						break binsLoop;
					}
				}
			}
			if(newBin)
			{
				bins.addNewBin();
				nBins++;
				//If we open a new Bin, it's sure current item will be there
				binTmp = Bin1D.class.cast(bins.getBin(nBins-1));
				int pos=0;
				binTmp.addItem(itemTmp, pos);
				i++;				
			}
		}
		
		
	}
	
	public static void nextFit1D()
	{
		methodUsed = "Next Fit 1D";

		int capacity =  Bins1D.class.cast(bins).getCapacityX();
		bins.addNewBin();
		Bin1D binTmp;
		Item1D itemTmp;
		int nBins = 1;
		int nItems = items.getNumberOfItems();
		for(int i=0; i<nItems;)
		{
			itemTmp = Item1D.class.cast(items.getItem(i));
			if(itemTmp.getLength()>capacity)
			{
				//TODO: Drop exception here
				i++;
				continue;
			}
			
			Boolean newBox;
			newBox = true;
			binTmp = Bin1D.class.cast(bins.getBin(nBins-1));
			
			if(itemTmp.getVolumen() > (1-binTmp.getOccupancy())*binTmp.getVolumen())
			{
				// Don't even try to search if not enough space in the bin
				bins.addNewBin();
				nBins++;
				continue;
			}
			for(int pos=0;pos<capacity;pos++)
			{	
				
				if(binTmp.isFreePosition(itemTmp, pos))
				{
					binTmp.addItem(itemTmp, pos);
					newBox=false;
					i++;
					break;
				}
			}
			if(newBox)
			{
				bins.addNewBin();
				nBins++;
				//If we open a new Bin, it's sure current item will be there
				binTmp = Bin1D.class.cast(bins.getBin(nBins-1));
				int pos=0;
				binTmp.addItem(itemTmp, pos);
				i++;	
			}
		}
	}
	
	
	public static void bestFit1D()
	{
		methodUsed = "Best Fit 1D";
		int capacity =  Bins1D.class.cast(bins).getCapacityX();
		bins.addNewBin();
		Bin1D binTmp;
		Item1D itemTmp;
		int nBins = 1;
		int nItems = items.getNumberOfItems();
		for(int i=0; i<nItems;)
		{
			itemTmp = Item1D.class.cast(items.getItem(i));
			if(itemTmp.getLength()>capacity)
			{
				//TODO: Drop exception here
				i++;
				continue;
			}
			Boolean newBox;
			newBox = true;
			
			int idxBin;
			binLoop: 
			for(int bin=0;bin<nBins;bin++)
			{	
				idxBin = bins.getIndexOfBinsByOccupancyDesc().get(bin);
				binTmp = Bin1D.class.cast(bins.getBin(idxBin));
				//TODO: verify (1-occ) posible discrepancies
				if(itemTmp.getVolumen() > (1-binTmp.getOccupancy())*binTmp.getVolumen())
				{
					// Don't even try to search if not enough space in the bin
					continue;
				}
				for(int pos=0;pos<capacity;pos++)
				{					
					if(binTmp.isFreePosition(itemTmp, pos))
					{
						binTmp.addItem(itemTmp, pos);
						newBox=false;
						i++;
						break binLoop;
					}
				}

			}
			
			if(newBox)
			{
				bins.addNewBin();
				nBins++;
				//If we open a new Bin, it's sure current item will be there
				binTmp = Bin1D.class.cast(bins.getBin(nBins-1));
				int pos=0;
				binTmp.addItem(itemTmp, pos);
				i++;	
			}
		}
				
	}
	
	
	public static void firsFit2D()
	{
		methodUsed = "First Fit 2D";
		if(rotation) methodUsed+= " (with Rotation)";

		int capacityX =  Bins2D.class.cast(bins).getCapacityX();
		int capacityY =  Bins2D.class.cast(bins).getCapacityY();

		bins.addNewBin();
		Bin2D binTmp;
		Item2D itemTmp;
		int nBins = 1;
		int nItems = items.getNumberOfItems();
		itemsLoop:
		for(int i=0; i<nItems; )
		{
			itemTmp = Item2D.class.cast(items.getItem(i));
			Boolean newBin;
			newBin = true;
			binLoop:
			for(int bin=0;bin<nBins;bin++)
			{
				binTmp = Bin2D.class.cast(bins.getBin(bin));

				if(   itemTmp.getWidth()>capacityX  ||  itemTmp.getHeight()>capacityY  )
				{
					i++;
					continue itemsLoop;
				}


				if(itemTmp.getVolumen() > (1-binTmp.getOccupancy())*binTmp.getVolumen())
				{
					// Don't even try to search if not enough space in the bin
					continue binLoop;
				}


				ArrayList<Integer> positionTmp =  new ArrayList<>();

				positionLoop: 
				for(int posX=0;posX<capacityX;posX++)
					for(int posY=0;posY<capacityY;posY++)				
				{				
					positionTmp.clear();
					positionTmp.add(posX);
					positionTmp.add(posY);
					if(binTmp.isFreePosition(itemTmp, positionTmp))
					{
						binTmp.addItem(itemTmp, positionTmp);
						//newBin=false;
						i++;
						//break positionLoop;
						//break binLoop;
						continue itemsLoop;
					}
					
					if(rotation)
					{
						itemTmp.rotate();
						if(binTmp.isFreePosition(itemTmp, positionTmp))
						{
							binTmp.addItem(itemTmp, positionTmp);
							i++;
							//break positionLoop;
							//break binLoop;
							continue itemsLoop;
						}
						itemTmp.rotate();
					}
					
					
						
				}
			}
			if(newBin)
			{
				bins.addNewBin();
				nBins++;
				//If we open a new Bin, it's sure current item will be there
				binTmp = Bin2D.class.cast(bins.getBin(nBins-1));
				ArrayList<Integer> positionTmp =  new ArrayList<>();
				positionTmp.add(0);
				positionTmp.add(0);
				binTmp.addItem(itemTmp, positionTmp);
				i++;
				
			}
		}
		
	}
	
	
	public static void nextFit2D()
	{
		methodUsed = "Next Fit 2D";
		if(rotation) methodUsed+= " (with Rotation)";

		int capacityX =  Bins2D.class.cast(bins).getCapacityX();
		int capacityY =  Bins2D.class.cast(bins).getCapacityY();

		bins.addNewBin();
		Bin2D binTmp;
		Item2D itemTmp;
		int nBins = 1;
		int nItems = items.getNumberOfItems();
		for(int i=0; i<nItems; )
		{
			itemTmp = Item2D.class.cast(items.getItem(i));
			Boolean newBin;
			newBin = true;
			binTmp = Bin2D.class.cast(bins.getBin(nBins-1));
			
			if(   itemTmp.getWidth()>capacityX  ||  itemTmp.getHeight()>capacityY  )
			{
				i++;
				continue;
			}
			
			
			if(itemTmp.getVolumen() > (1-binTmp.getOccupancy())*binTmp.getVolumen())
			{
				// Don't even try to search if not enough space in the bin
				bins.addNewBin();
				nBins++;
				continue;
			}
			
			
			ArrayList<Integer> positionTmp =  new ArrayList<>();
			
			positionLoop: 
			for(int posX=0;posX<capacityX;posX++)
				for(int posY=0;posY<capacityY;posY++)				
			{				
				positionTmp.clear();
				positionTmp.add(posX);
				positionTmp.add(posY);
				if(binTmp.isFreePosition(itemTmp, positionTmp))
				{
					binTmp.addItem(itemTmp, positionTmp);
					newBin=false;
					i++;
					break positionLoop;
				}
				if(rotation)
				{
					itemTmp.rotate();
					if(binTmp.isFreePosition(itemTmp, positionTmp))
					{
						binTmp.addItem(itemTmp, positionTmp);
						newBin=false;
						i++;
						break positionLoop;
					}
					itemTmp.rotate();
				}
			}
			if(newBin)
			{
				bins.addNewBin();
				nBins++;
				//If we open a new Bin, it's sure current item will be there
				binTmp = Bin2D.class.cast(bins.getBin(nBins-1));
				ArrayList<Integer> pTmp =  new ArrayList<>();
				pTmp.add(0);
				pTmp.add(0);
				binTmp.addItem(itemTmp, pTmp);
				i++;
			}
		}
	}
	
	
	public static void bestFit2D()
	{
		methodUsed = "Best Fit 2D";
		if(rotation) methodUsed+= " (with Rotation)";

		int capacityX =  Bins2D.class.cast(bins).getCapacityX();
		int capacityY =  Bins2D.class.cast(bins).getCapacityY();
		
		bins.addNewBin();
		Bin2D binTmp;
		Item2D itemTmp;
		int nBins = 1;
		int nItems = items.getNumberOfItems();
		for(int i=0; i<nItems;)
		{
			itemTmp = Item2D.class.cast(items.getItem(i));

			if(itemTmp.getWidth()>capacityX || itemTmp.getHeight()>capacityY)
			{
				//TODO: Drop exception here -> Item too big for bin
				i++;
				continue;
			}

			
			Boolean newBin;
			newBin = true;
			
			int idxBin;
			binLoop: 
			for(int bin=0;bin<nBins;bin++)
			{	
				idxBin = bins.getIndexOfBinsByOccupancyDesc().get(bin);
				binTmp = Bin2D.class.cast(bins.getBin(idxBin));
				ArrayList<Integer> posTmp = new ArrayList<>();
				//TODO: verify (1-occ) discrepancies
				if(itemTmp.getVolumen() > (1-binTmp.getOccupancy())*binTmp.getVolumen())
				{
					// Don't even try to search if not enough space in the bin
					continue;
				}
				for(int posY=0;posY<capacityY;posY++)
					for(int posX=0;posX<capacityX;posX++)
				{					
					posTmp.clear();
					posTmp.add(posX);
					posTmp.add(posY);
					if(binTmp.isFreePosition(itemTmp, posTmp))
					{
						binTmp.addItem(itemTmp, posTmp);
						newBin=false;
						i++;
						break binLoop;
					}
					
					if(rotation)
					{
						itemTmp.rotate();
						if(binTmp.isFreePosition(itemTmp, posTmp))
						{
							binTmp.addItem(itemTmp, posTmp);
							newBin=false;
							i++;
							break binLoop;
						}
						itemTmp.rotate();
					}
					
					
					
				}

			}
			
			if(newBin)
			{
				bins.addNewBin();
				nBins++;
				//If we open a new Bin, it's sure current item will be there
				binTmp = Bin2D.class.cast(bins.getBin(nBins-1));
				ArrayList<Integer> pTmp =  new ArrayList<>();
				pTmp.add(0);
				pTmp.add(0);
				binTmp.addItem(itemTmp, pTmp);
				i++;
			}
		}
		
		
	}
	
	
	
	public static void firstFitDecreasing1D()
	{
		items.sortItemsByVolumenDesc();
		//items.sortItemsById();
		firstFit1D();
		methodUsed = "First Fit Decreasing 1D";
	}
	
	public static void bestFitDecreasing1D()
	{
		items.sortItemsByVolumenDesc();
		bestFit1D();
		methodUsed = "Best Fit Decreasing 1D";
	}
	
	public static void nextFitDecreasing1D()
	{
		items.sortItemsByVolumenDesc();
		//items.sortItemsById();
		firstFit1D();
		methodUsed = "Next Fit Decreasing 1D";
	}
	
	public static void firstFitDecreasing2D()
	{
		items.sortItemsByVolumenDesc();
		//items.sortItemsById();
		firsFit2D();
		methodUsed = "First Fit Decreasing 2D";
		if(rotation) methodUsed+= " (with Rotation)";

	}
	

	public static void bestFitDecreasing2D()
	{
		items.sortItemsByVolumenDesc();
		bestFit2D();
		methodUsed = "Best Fit Decreasing 2D";
		if(rotation) methodUsed+= " (with Rotation)";

	}
	
	public static void nextFitDecreasing2D()
	{
		items.sortItemsByVolumenDesc();
		nextFit2D();
		methodUsed = "Next Fit Decreasing 2D";
			if(rotation) methodUsed+= " (with Rotation)";

	}
	
	public static void firstFitWidthDecreasing2D()
	{
		items.sortItemsByWidthDesc();
		firsFit2D();
		methodUsed = "First Fit Width-Decreasing 2D";
	}
	

	public static void bestFitWidthDecreasing2D()
	{
		items.sortItemsByWidthDesc();
		bestFit2D();
		methodUsed = "Best Fit Width-Decreasing 2D";
	}
	
	public static void nextFitWidthDecreasing2D()
	{
		items.sortItemsByWidthDesc();
		nextFit2D();
		methodUsed = "Next Fit Width-Decreasing 2D";
	}
	
	public static void firstFitHeightDecreasing2D()
	{
		items.sortItemsByHeightDesc();
		firsFit2D();
		methodUsed = "First Fit Height-Decreasing 2D";
	}
	

	public static void bestFitHeightDecreasing2D()
	{
		items.sortItemsByHeightDesc();
		bestFit2D();
		methodUsed = "Best Fit Height-Decreasing 2D";
	}
	
	public static void nextFitHeightDecreasing2D()
	{
		items.sortItemsByHeightDesc();
		nextFit2D();
		methodUsed = "Next Fit Height-Decreasing 2D";
	}
	
	
	public static String generateReport1D()
	{
		String report;
		report = "BIN PACKING REPORT\n";
		report+= "Method used:\t"+methodUsed+"\n";
		report+= "Number of Dimensions:\t"+numberOfDimensions+"\n";
		report+= "Number of bins used:\t";
		report+= bins.getNumberOfBins();
		report+= "\n";
		
		report+= "Items Distribution:\n";
		report+= "ITEM \tBIN \tPOS \tLENGTH";
		for(int i=0;i<items.getNumberOfItems();i++)
		{
			report+= "\n";			
			report+= items.getItem(i).getID();
			report+= "\t";
			int index = bins.getIndexOfBin(Bin.itemsBinsMap.get(items.getItem(i).getID()));
			if(index == -1)
			{
				report+="null\tnull\tnull";
				break;
			}
			report+= String.valueOf(index+1);
			report+= "\t";
			report+= String.valueOf(Bin.itemsPositions.get(items.getItem(i).getID()).get(0));
			report+= "\t";
			report+= String.valueOf(items.getItem(i).getDimensions().get(0));
		}
		
		report+="\n";
		report+="\nBins Content:\n";
		report+="Bin capacity = " + bins.getCapacityBins().get(0)+"\n";
		report+= "BIN \tITEM \tPOSX\tLENGTH\n";
		for(int i=0;i<bins.getNumberOfBins();i++)
		{
			report+= String.valueOf(i+1);
			if(bins.getBin(i).getNumberOfItemsContained()==0)
			{
				report+= "\tnull\tnull\tnull\n";
			}
			for(int j=0;j<bins.getBin(i).getNumberOfItemsContained();j++)
			{
				report+="\t";
				report+= bins.getBin(i).getItem(j).getID();
				report+= "\t";
				report+= String.valueOf(Bin.itemsPositions.get(bins.getBin(i).getItem(j).getID()).get(0));
				report+= "\t";
				report+= bins.getBin(i).getItem(j).getDimensions().get(0);				
				report+= "\n";
			}
		}
				
		report+="Bins Occupancies\n";
		report+= "BIN \tOCCUPANCY\n";
		for(int i=0;i<bins.getNumberOfBins();i++)
		{
			report+=String.valueOf(i+1)+"\t";
			report+= String.valueOf(bins.getBin(i).getOccupancy())+"\n";
		}
		
		if(reportFilePath != null)
		{
			try {
				PrintWriter out = new PrintWriter(reportFilePath);
				out.println(report);
				out.close();
			} catch (FileNotFoundException ex) {
				ex.printStackTrace();
			}
		}
		

		return report;
	}
	
	public static String generateReport2D()
	{
		String report;
		report = "BIN PACKING REPORT\n";
		report+= "Method used:\t"+methodUsed+"\n";
		report+= "Number of Dimensions:\t"+numberOfDimensions+"\n";
		report+= "Number of bins used:\t";
		report+= bins.getNumberOfBins();
		report+= "\n";
		
		report+= "Items Distribution:\n";
		report+= "ITEM \tBIN \tPOSX\tPOSY\tWIDTH\tHEIGHT";
		for(int i=0;i<items.getNumberOfItems();i++)
		{
			report+= "\n";			
			report+= items.getItem(i).getID();
			report+= "\t";
			int index = bins.getIndexOfBin(Bin.itemsBinsMap.get(items.getItem(i).getID()));
			if(index>=0 )
			{
				report+= String.valueOf(index+1);
				report+= "\t";
				report+= String.valueOf(Bin.itemsPositions.get(items.getItem(i).getID()).get(0));
				report+= "\t";
				report+= String.valueOf(Bin.itemsPositions.get(items.getItem(i).getID()).get(1));
				report+= "\t";
			} else
			{
				report+="null\tnull\tnull\t";
			}
			
			report+=items.getItem(i).getDimensions().get(0)+"\t";
			report+=items.getItem(i).getDimensions().get(1);

		}
		
		report+="\n";
		report+="\nBins Content:\n";
		report+="Bin width = " + bins.getCapacityBins().get(0)+"\n";
		report+="Bin heigth = " + bins.getCapacityBins().get(1)+"\n";
		report+= "BIN \tITEM \tPOSX\tPOSY\tWIDTH\tHEIGHT\n";
		for(int i=0;i<bins.getNumberOfBins();i++)
		{
			
			report+= String.valueOf(i+1);
			//report+= "\t";
			if(bins.getBin(i).getNumberOfItemsContained()==0)
			{
				report+= "\tnull\tnull\tnull\tnull\tnull\n";
			}
			for(int j=0;j<bins.getBin(i).getNumberOfItemsContained();j++)
			{
				report+="\t";
				report+= bins.getBin(i).getItem(j).getID();
				report+= "\t";
				report+=String.valueOf(Bin.itemsPositions.get(bins.getBin(i).getItem(j).getID()).get(0));
				report+= "\t";
				report+=String.valueOf(Bin.itemsPositions.get(bins.getBin(i).getItem(j).getID()).get(1));
				report+= "\t";
				report+= bins.getBin(i).getItem(j).getDimensions().get(0);
				report+= "\t";
				report+= bins.getBin(i).getItem(j).getDimensions().get(1);
				report+= "\n";
			}
		}
		
		report+="Bins Occupancies\n";
		report+= "BIN \tOCCUPANCY\n";
		for(int i=0;i<bins.getNumberOfBins();i++)
		{
			report+=String.valueOf(i+1)+"\t";
			report+= String.valueOf(bins.getBin(i).getOccupancy())+"\n";
		}
		if(reportFilePath != null)
		{
			try 
			{
				PrintWriter out = new PrintWriter(reportFilePath);
				out.println(report);
				out.close();
			}
			catch (FileNotFoundException ex) 
			{
				ex.printStackTrace();
			}
		}
		
		return report;
	}
}
